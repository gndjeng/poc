# README #

Demo

### Tech ###

* [Web Components](https://www.w3.org/standards/techs/components#w3c_all) - Web Components Spec
* [Custom Elements](https://customelements.io/) - Custom Elements built by the community
* [Polymer](https://www.polymer-project.org/1.0/) - Build a better web!
* [node.js](https://nodejs.org/en/) 
* [Gulp](https://gulpjs.com/) - Build Streams



### How do I get set up? ###

For Windows users, download and install the following before getting started

* [node.js](https://nodejs.org/en/)
* [Git Bash](https://git-for-windows.github.io/) - Git commands for Windows 8 and below


```sh
$ npm install -g gulp  bower polymer-cli
$ git clone https://bitbucket.org/gndjeng/poc.git
$ cd Polymer
$ npm install -d && bower install
$ npm start
```

### Contribution guidelines ###

Each component or Polymer Element can  be tested. Popular libraries for testing web components

* [Jasmine](https://jasmine.github.io/)
* [Mocha](https://mochajs.org/)
* [Polymer Test Spec](https://www.polymer-project.org/1.0/docs/tools/tests)