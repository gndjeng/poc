# README #

Demo

### Tech ###

* [Web Components](https://www.polymer-project.org/1.0/) - Components
* [Polymer](https://www.polymer-project.org/1.0/) - Build a better web!
* [Twitter Bootstrap](https://v4-alpha.getbootstrap.com/) - UI for modern web apps
* [node.js](https://nodejs.org/en/) - evented I/O for the backend
* [GulpJS](https://gulpjs.com/) - Build Streams



### How do I get set up? ###

For Windows users, download and install the following before getting started


* [node.js](https://nodejs.org/en/)
* [Git Bash](https://git-for-windows.github.io/) - Git commands for Windows 8 and below
* [WebStorm](https://www.jetbrains.com/webstorm/) - Best IDE for JavaScript




```sh
$ git clone https://bitbucket.org/gndjeng/poc.git
$ cd Polymer
$ npm install -d && bower install
$ npm start
```



### How to run tests ###


### Contribution guidelines ###

Each component or Polymer Element can  be tested. Popular libraries for testing web components

* [Jasmine](https://jasmine.github.io/)
* [Mocha](https://mochajs.org/)
* [Polymer Test Spec](https://www.polymer-project.org/1.0/docs/tools/tests)



### Code review ###
