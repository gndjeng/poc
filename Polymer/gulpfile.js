/*
    Just for testing - Move to Webpack instead
 */

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    del = require('del');


gulp.task('styles', function() {
    return gulp.src('assets/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css'))
        .pipe(notify({ message:'SCSS files compiled!' }));
});

gulp.task('scripts', function() {
    return gulp.src('assets/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('bundle.js')) // merging all js files into one main.js file
        .pipe(rename({ suffix: '.min' })) //creating minified version
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(notify({ message: 'JS files minified!' }));
});

gulp.task('images', function(){
    return gulp.src('assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});


gulp.task('watch', function() {
    gulp.watch('assets/scss/**/*.scss', ['styles']);
    gulp.watch('assets/js/**/*.js', ['scripts']);
});


gulp.task('incremental', function() {
    gulp.start('styles', 'images');
});


gulp.task('default',  function() {
    gulp.start('styles', 'images', 'scripts');
});

